****************
SERVEUR JEE
****************
1) lancer un serveur JEE configuré avec une source de données

2) Dans le fichier src/ejb/entites/persistence.xml, indiquez le nom JNDI de cette source de données
  (par défaut java:/PostgresDS)
  
****************
 CONFIGURATION
***************
3) Modifiez les lignes 5-10 du fichier build.xml

****************
Exécution
****************
4) compiler, construire l'application JEE et la déployer  via la commande
ant deployEntityAndSessionWithEAR

5) peupler la base avec la commande
ant initBase

6) lancer le programme client avec la commande

ant run-client

