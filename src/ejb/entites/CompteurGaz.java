package ejb.entites;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

@Entity 
@DiscriminatorValue(value="compteurgaz")
public class CompteurGaz extends EquipementElectrique {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CompteurGaz() {
	}

}
