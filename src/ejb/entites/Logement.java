package ejb.entites;

import java.util.Set;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity public class Logement  implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id private String code ;
	private String adresse ;
	@OneToMany private Set<Piece> pieces;
	public Logement() {}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public Set<Piece> getPieces() {
		return pieces;
	}
	public void setPieces(Set<Piece> pieces) {
		this.pieces = pieces;
	}
}
