package ejb.entites;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity public class Piece   implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id private int id ;
	private TypePiece typePiece ;
	private double superficie ;
	
	public Piece() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TypePiece getTypePiece() {
		return typePiece;
	}

	public void setTypePiece(TypePiece typePiece) {
		this.typePiece = typePiece;
	}

	public double getSuperficie() {
		return superficie;
	}

	public void setSuperficie(double superficie) {
		this.superficie = superficie;
	}
}
