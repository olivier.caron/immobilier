package ejb.sessions;

import java.util.Collection;
import java.util.List;

import jakarta.persistence.NoResultException;

import ejb.entites.EquipementElectrique;
import ejb.entites.Locataire;
import ejb.entites.Logement;
import ejb.entites.Piece;


@jakarta.ejb.Remote public interface IGestionAgence {

	public Locataire testPersist(String code,String nom, String prenom,String email);

	void newLogement(String code, String adr);

	void testMerge(Logement l);

	Collection<Locataire> getLocatairesParNom(String debutMot) ;
	Collection<Locataire> getLocatairesParNomV2(String debutMot) ;
	Collection<Logement> getLogementsOccupesV1() ;
	Collection<Logement> getLogementsOccupesV2() ;
	
	public Collection<Piece> getPiecesLogementsOccupesV1();
	public Collection<Piece> getPiecesLogementsOccupesV2();
	
	double getSuperficieLogementsOccupes() throws NoResultException;
	
	Collection<EquipementElectrique> getEquipementsDuLogement(String codeLogement)
	throws LogementInconnuException ;

	List<Object[]> getNbPiecesParLogements();
}
