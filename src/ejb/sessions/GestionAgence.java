package ejb.sessions;


import java.util.Collection;
import java.util.List;

import jakarta.ejb.Stateless;

import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.PersistenceException;
import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;
import ejb.entites.EquipementElectrique;

import ejb.entites.Locataire;
import ejb.entites.Logement;
import ejb.entites.Piece;





@Stateless
public class GestionAgence implements IGestionAgence {
    @PersistenceContext(unitName="agence-unit") EntityManager em ;
	
    public GestionAgence() {	
	}
    @Override
    public void newLogement
    (String code, String adr) {
     try {
     Logement l = new Logement();
     l.setCode(code);
     l.setAdresse(adr) ;
     em.persist(l) ; em.flush();
     System.out.println("------------------> ok:"+code);
     } catch (Exception e) {System.out.println("failed") ; }
    }
    
    @Override
	public void testMerge(Logement l) {
    	em.merge(l) ;
    }
	@Override
    public Locataire testPersist(String code,String nom, String prenom,String email) 
	 {
		Locataire loc=new Locataire();
		loc.setCode(code); loc.setEmail(email);
		loc.setNom(nom); loc.setPrenom(prenom);
		try {
		  em.persist(loc);
		  System.out.println("----------------------> ok 1") ;
		  em.flush();
		  System.out.println("----------------------> ok 2") ;
		} catch (PersistenceException e) {
			System.out.println("----------> détection duplication entité - serveur") ;
			System.out.println(e.getClass());
		}
		return loc ;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Locataire> getLocatairesParNom(String debutMot) {
		String reqJPQL="select loc from Locataire loc where loc.nom like :pattern" ;
		Query q=em.createQuery(reqJPQL).setParameter("pattern",debutMot+"%");
		return (Collection<Locataire>)q.getResultList();	
	}
	
	
	@Override
	public Collection<Locataire> getLocatairesParNomV2(String debutMot) {
		String reqJPQL="select loc from Locataire loc where loc.nom like :pattern" ;
		TypedQuery<Locataire> q=em.createQuery(reqJPQL, Locataire.class).setParameter("pattern",debutMot+"%");
		return (Collection<Locataire>)q.getResultList();	
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Logement> getLogementsOccupesV1() {
		String reqJPQL="select loc.logement from Locataire loc" ;
		Query q=em.createQuery(reqJPQL);
		return (Collection<Logement>)q.getResultList();
	}
	
	
	@Override
	public Collection<Logement> getLogementsOccupesV2() {
		String reqJPQL="select logementsOccupes from Locataire loc join loc.logement logementsOccupes"  ;
		TypedQuery<Logement> q=em.createQuery(reqJPQL, Logement.class);
		return (Collection<Logement>)q.getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Piece> getPiecesLogementsOccupesV1() {
		String reqJPQL="select loc.logement.pieces from Locataire loc" ;
		Query q=em.createQuery(reqJPQL);
		return (Collection<Piece>) q.getResultList();
	}
	
	
	@Override
	public Collection<Piece> getPiecesLogementsOccupesV2() {
		String reqJPQL="select pieces from Locataire loc "+
	                   "join loc.logement logementsOccupes "+
				       "join logementsOccupes.pieces pieces";
		TypedQuery<Piece> q=em.createQuery(reqJPQL,Piece.class);
		return (Collection<Piece>)q.getResultList();
	}
	
	
	
	@Override
	public double getSuperficieLogementsOccupes() throws NoResultException {
		String reqJPQL="select sum(p.superficie) "+
	    " from Locataire loc join loc.logement lo join lo.pieces p"  ;
		Query q=em.createQuery(reqJPQL);
		return (Double) q.getSingleResult();
	}
	
	@Override
	public Collection<EquipementElectrique> getEquipementsDuLogement(String codeLogement)
			throws LogementInconnuException {
		Logement log= (Logement) em.find(Logement.class, codeLogement);
		if (log==null) throw new LogementInconnuException() ;
		String reqJPQL="select equip "+
			    " from Logement lo join lo.pieces p "+
				" join EquipementElectrique equip on equip.piece=p "+
			    " where lo.code=:code";
				TypedQuery<EquipementElectrique> q=em.createQuery(reqJPQL,EquipementElectrique.class);
				q.setParameter("code", codeLogement);
				return (Collection<EquipementElectrique>) q.getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getNbPiecesParLogements(){
		String reqJPQL="select lo, count(*) as nombre  from Logement lo join lo.pieces p group by lo";
		Query q=em.createQuery(reqJPQL) ;
		List<Object[]> l= (List<Object []>) q.getResultList();
		return l ;
	}
    
	
}
