package client;
import jakarta.ejb.EJBException;
import javax.naming.InitialContext;
import javax.naming.NamingException ;


import ejb.entites.EquipementElectrique;
import ejb.entites.Locataire;
import ejb.entites.Logement;
import ejb.entites.Piece;
import ejb.sessions.IGestionAgence;
import ejb.sessions.LogementInconnuException;
public class Main {


	public static void main(String[] args) {
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			Object obj ;
		    obj = ctx.lookup("ejb:immobilier/immobilierSessions//GestionAgence!ejb.sessions.IGestionAgence");
		    IGestionAgence serv = (IGestionAgence) obj ;
		    
		    System.out.print("create l1 ") ;
		    
		    serv.newLogement("l1", "adr");
		    System.out.print("success");
		    Logement l1= new Logement() ; l1.setCode("l1") ; l1.setAdresse("newadr");
		    Logement l2=new Logement() ; l2.setCode("l2") ; l2.setAdresse("bidon");
		    serv.testMerge(l1); 
		    serv.testMerge(l2); 
		    System.out.println();
		    System.out.println("Les locataires dont le nom commencent par 'D' :");
		    for (Locataire loc : serv.getLocatairesParNom("D"))
		    	System.out.println("Nom Prénom: "+loc.getNom()+" "+loc.getPrenom());
		    System.out.println("Les locataires dont le nom commencent par 'D' :");
		    for (Locataire loc : serv.getLocatairesParNomV2("D"))
		    	System.out.println("Nom Prénom: "+loc.getNom()+" "+loc.getPrenom());
		    System.out.println("Les logements occupés, version 1 :");
		    for (Logement lg : serv.getLogementsOccupesV1())
		    	System.out.println("code logement: "+lg.getCode());
		    System.out.println("Les logements occupés, version 2 :");
		    for (Logement lg : serv.getLogementsOccupesV2())
		    	System.out.println("code logement: "+lg.getCode());
				/*
				 * System.out.println("Les pièces des logements occupés, version 1 :");
				 * for (Piece p : serv.getPiecesLogementsOccupesV1())
				 * System.out.println("code logement: "+p.getId()+" - "+p.getSuperficie(
				 * ));
				 */System.out.println("Les pièces des logements occupés, version 2 :");
		    for (Piece p : serv.getPiecesLogementsOccupesV2())
		    	System.out.println("code logement: "+p.getId()+" - "+p.getSuperficie());
		    System.out.print("Superficie des logements occupés : ");
		    System.out.println(serv.getSuperficieLogementsOccupes());
		    System.out.println("Les équipements du logement Log01 : ");
		    for (EquipementElectrique e : serv.getEquipementsDuLogement("Log01"))
		    	System.out.println("Equip: "+e);
		    System.out.println("Les logements et leur pièces:");
		    for (Object ligne[] : serv.getNbPiecesParLogements()) {
		    	Logement lg= (Logement) ligne[0] ; long nb = (Long) ligne[1];
		    	System.out.println("Logement "+lg.getCode()+" : "+nb+" pièce(s)");
		    }
		} catch(EJBException e) {
		    System.err.print("failed");  
		     /* try {
		    	  serv.testPersist("loc54", "Smith", "John", "jsmith@polytech-lille.fr");
		    	  System.out.println("testPersist loc54 OK");
		      } catch(EJBException e1) {
		    	  System.err.println("erreur sur premier testPersist"+e1.getMessage());
		      }
		      try {
		    	  serv.testPersist("loc54", "Smith", "John", "jsmith@polytech-lille.fr");
		    	  System.out.println("testPersist loc54 OK");
		      } catch(EJBException e1) {
		    	  System.err.println("erreur sur second testPersist"+e1.getMessage());
		    	  System.err.println(e1.getClass());
		      } */ 
		} catch (LogementInconnuException e) {
			System.err.println("logement inconnu");
		}catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}     
	}
}
