


insert into logement (code, adresse) values
  ('Log01','10 rue des Lilas, 59000 Lille'),
  ('Log02','15 rue de Turenne, 59000 Lille'),
  ('Log03','20 rue Faidherbe, 59000 Lille') ;

 insert into locataire (code, email, nom, prenom, logement_code) values
  ('loc347',	'dubois@hotmail.fr','Dubois','Pierre',null),
  ('loc227',	'dupont@free.fr',	'Dupont',	'Jean','Log01'),
  ('loc321',	'durant@yahoo.fr','Durant',	'Paul', 'Log02') ;


insert into piece(id,superficie,typepiece) values
 (1,6,2),(2,12,3),(3,5,4),(4,20,5),(5,1.5,7),(6,3,8),
 (7,3,2),(8,10,3),(9,11,3),(10,4,4),(11,8,5),(12,6,6),(13,2,7),(14,20,9),
 (15,6,1),(16,10,2),(17,12,3),(18,6,4), (19,20,6),(20,5,10) ;
 
insert into logement_piece(logement_code,pieces_id) values
 ('Log01',1),('Log01',2),('Log01',3),('Log01',4),('Log01',5),('Log01',6),
 ('Log02',7),('Log02',8),('Log02',9),('Log02',10),('Log02',11),('Log02',12),('Log02',13),('Log02',14),
 ('Log03',15),('Log03',16),('Log03',17),('Log03',18),('Log03',19),('Log03',20) ;
 
insert into equipementelectrique(dtype,code,piece_id) values
 ('compteuredf',1,6),('compteurgaz',2,6),('boitiertelecom',3,4),
 ('compteuredf',4,7),('boitiertelecom',5,7),
 ('compteuredf',6,15),('compteurgaz',7,15),('boitiertelecom',8,15) ;
 
